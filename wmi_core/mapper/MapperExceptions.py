class ConnectionException(Exception):
    """ Exceção lançada quando ocorrer falhas de onecção com WNetAddConnection2 """
    def __init__(self, message):
        self.message = message

    @property
    def strerror(self):
        return self.message

    def __str__(self):
        return self.message


class DisconectException(ConnectionException):
    pass



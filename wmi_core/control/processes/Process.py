# -*- coding: UTF-8 -*-
import wmi
from win32api import FormatMessage
from typing import (
    List,
    Dict
)

from .ProcessExceptions import (
    NotValidWmiNamespaceInstance,
    ReadProcessError,
    InitProcessError,
    WatchProcessError
)


class Process(object):
    """
    Classe para controlar os processos de um PC Windows
    """

    SW_HIDE = 0
    SW_NORMAL = 1
    SW_SHOWMINIMIZED = 2
    SW_SHOWMAXIMIZED = 3
    SW_SHOWNOACTIVATE = 4
    SW_SHOW = 5
    SW_MINIMIZE = 6
    SW_SHOWMINNOACTIVE = 7
    SW_SHOWNA = 8
    SW_RESTORE = 9
    SW_SHOWDEFAULT = 10
    SW_FORCEMINIMIZE = 11

    def __init__(self, _wmi_namespace: object) -> None:
        """
        Método inicializador da classe
        :param _wmi_namespace:  Uma instancia de wmi.WMI() com o namespace
                                root/cimv2
        """
        if not isinstance(_wmi_namespace, wmi._wmi_namespace):
            raise NotValidWmiNamespaceInstance("A instância de Process não recebeu um wmi_namespace válido")
        self.wmi = _wmi_namespace

    @property
    def properties(self) -> List:
        """
        Retorna um dicionário com os campos de uma instancia de
        Win32_Process
        :return:
        """

        return list(self.wmi.Win32_Process()[0].properties)

    def __wmi_object_to_dict(self, wmi_object: wmi._wmi_object) -> Dict:
        """
        Método privado para converter um objeto wmi._wmi_object
        em um dicionario com as propriedades da classe Win32_Process
        Para saber quais campos será retornado neste dicionario use
        o método __keys__

        :param wmi_object:  Uma instancia de wmi._wmi_object
        :return:            Dicionario com as propriedades da classe

        """
        if not isinstance(wmi_object, wmi._wmi_object):
            raise TypeError(
                'O parametro do método __wmi_object_to_dict deve ser uma instancia de wmi._wmi_object'
            )

        properties_names = wmi_object.__dict__['properties'].keys()
        properties_values = [getattr(wmi_object, property_name) for property_name in properties_names]
        return dict(
            zip(
                properties_names,
                properties_values
            )
        )

    def list_process(self):
        """
        Método para listar todas os processos de um PC Windows
        :return:
        """
        list_process = []
        try:
            for process in self.wmi.Win32_Process():
                list_process.append(self.__wmi_object_to_dict(process))
            return list_process
        except Exception:
            raise ReadProcessError(
                'Falha ao coletar os processos do PC'
            )

    def get_process_by_id(self, process_id: int) -> Dict:
        """
        Método para coletar informações para um processo corrente

        :param process_id:      Um ID de um processo corrente
        :return:                Dicionario com as informações do processo

        """
        try:

            if type(process_id) is not int:
                raise TypeError(
                    'O id do processo informado não é um int com dígitos válidos'
                )

            for process in self.wmi.Win32_Process(ProcessId=process_id):
                return self.__wmi_object_to_dict(process)
        except Exception:
            raise ReadProcessError(
                'Falha ao coletar os processos do PC com o id %d' % process_id
            )

    def get_process_like_name(self, process_name: str) -> List[Dict]:
        """
        Método para coletar informações de um processo filtrando
        a consulta pelo nome do processo.

        :param process_name:    O nome do processo corrente
        :return:                Uma lista com dicionarios com
                                detalhes do processo.
        """
        try:

            if type(process_name) is not str:
                raise TypeError(
                    'O nome do processo informado não é uma str válida'
                )

            list_process = []

            wquery = "SELECT * FROM Win32_Process WHERE Caption LIKE '%{}%'".format(process_name)

            for process in self.wmi.query(wquery):
                list_process.append(self.__wmi_object_to_dict(process))

            return list_process
        except Exception:
            raise ReadProcessError(
                'Falha ao coletar os processos do PC com o nome %s' % process_name
            )

    def init_process(self, command: str) -> Dict:
        """
        Método para iniciar um processo por um comando
        :param command:     Um comando válido no sistema
        :return:            Um dicionário com as informações do processo
        """
        try:
            if not isinstance(command, str):
                raise TypeError(
                    'O comando deve ser uma str'
                )

            startup = self.wmi.Win32_ProcessStartup.new(ShowWindow=self.SW_NORMAL)
            pid, retval = self.wmi.Win32_Process.Create(
                CommandLine=command,
                ProcessStartupInformation=startup
            )

            if self.__format_return_code(retval):
                return self.get_process_by_id(pid)

        except Exception:
            raise InitProcessError(
                'Falha ao iniciar o processo com o comando %s' % command
            )

    def stop_process_by_id(self, process_id: int) -> bool:
        """
        Método para parar um processo por um ID
        :param process_id:      ID do processo
        :return:                Bool
        :raise:                 TypeError, OSError, ReadProcessError
        """
        try:

            if type(process_id) is not int:
                raise TypeError(
                    'O id do processo informado não é um int com dígitos válidos'
                )

            for process in self.wmi.Win32_Process(ProcessId=process_id):
                rcode, = process.Terminate()
                return self.__format_return_code(rcode)

        except Exception:
            raise ReadProcessError(
                'Falha ao parar o processo do PC com o id %d' % process_id
            )

    def stop_all_process_like_name(self, process_name: str) -> bool:
        """
        Método para parar todos os processos correspondente ao nome
        informado
        :param process_name:        Um nome de um processo no sistema
        :return:                    bool
        """
        try:

            if type(process_name) is not str:
                raise TypeError(
                    'O nome do processo informado não é uma str válida'
                )

            wquery = "SELECT * FROM Win32_Process WHERE Caption LIKE '%{}%'".format(process_name)

            for process in self.wmi.query(wquery):
                rcode, = process.Terminate()
                self.__format_return_code(rcode)

            return True
        except Exception:
            raise ReadProcessError(
                'Falha ao parar os processos do PC com o nome %s' % process_name
            )

    def watch_process(self, process_name: str, notification_type: str ="Operation") -> Dict:
        """
        Método para monitoramento de status do processo.

        :param process_name:        Um Caption Name de um processo corrente
        :param notification_type:   Um tipo de notificação do processo, refere-se ao status do
                                    processo corrente. Os valores possíveis são:
                                    Deletion, Creation, Operation, Modification
                                    Por padrão o valor é Operation
        :return:
        """
        try:
            watcher = self.wmi.watch_for(
                notification_type=notification_type,
                wmi_class="Win32_Process",
                delay_secs=1,
                Caption=process_name
            )
            return self.__wmi_object_to_dict(wmi._wmi_object(watcher().ole_object))

        except Exception:
            raise WatchProcessError(
                'Falha ao monitorar o processo com Caption %s' % process_name
            )

    def get_owner_process_by_id(self, process_id: int) -> Dict:
        """
        Método para coletar informações do dono do processo corrente

        :param process_id:      Um ID de um processo corrente
        :return:                Dicionario com as informações do processo

        """
        try:

            if type(process_id) is not int:
                raise TypeError(
                    'O id do processo informado não é um int com dígitos válidos'
                )

            for process in self.wmi.Win32_Process(ProcessId=process_id):
                domain, rcode, username = process.GetOwner()
                if self.__format_return_code(rcode):
                    return {'username': username, 'domain': domain}

        except Exception:
            raise ReadProcessError(
                'Falha ao coletar o dono do processo do PC com o id %d' % process_id
            )

    def __format_return_code(self, return_code: int) -> bool:
        """
        Formata os códigos de retorno do sistema
        :param return_code:         Um inteiro com o retorno do sistema
        :return:                    bool
        :raise:                     OSError
        """
        rc = FormatMessage(return_code)
        if return_code > 0:
            raise OSError(rc)
        return True

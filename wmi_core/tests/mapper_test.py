# -*- coding: UTF-8 -*-
from unittest import TestCase, main
from ..mapper.mapper import NetResource, Mapper
import win32wnet


class MapperTest(TestCase):
    PASSWORD = None
    USERNAME = r"lmcorp\andre.matias"

    def tearDown(self):
        Mapper.unmap()

    def test_net_resource(self):
        resource = NetResource(r"\\10.228.1.144\LMCTI\Data")
        self.assertIsInstance(resource, win32wnet.NETRESOURCEType)

    def test_map(self):
        mapper = Mapper(NetResource(r"\\10.228.1.144\C$"), self.PASSWORD, self.USERNAME)
        self.assertTrue(mapper.map())

    def test_list(self):
        mapper = Mapper(NetResource(r"\\10.228.1.144\C$"), self.PASSWORD, self.USERNAME)
        self.assertTrue(mapper.map())
        self.assertTrue(len(Mapper.list()) > 0)

    def test_is_mapped_method(self):
        mapper = Mapper(NetResource(r"\\10.228.1.144\D$"), self.PASSWORD, self.USERNAME)
        self.assertTrue(mapper.map())
        self.assertTrue(Mapper.is_mapped(NetResource(r"\\10.228.1.144\D$")))

    def test_unmap(self):
        mapper = Mapper(NetResource(r"\\10.228.1.144\D$", 'K'), self.PASSWORD, self.USERNAME)
        self.assertTrue(mapper.map())
        self.assertTrue(Mapper.unmap_an(NetResource(r"\\10.228.1.144\D$", 'K')))
        self.assertTrue(Mapper.unmap())


if __name__ is "__main__":
    main()
